function logSubmit(event) {
  event.preventDefault(); // Evitar el envío del formulario

  // Obtener los valores de los campos del formulario
  const day = document.getElementById("day").value;
  const month = document.getElementById("month").value;
  const year = document.getElementById("year").value;

  // Obtener id errorMessage
  const errorMessageDay = document.getElementById("errorMessageDay");
  const errorMessageMonth = document.getElementById("errorMessageMonth");
  const errorMessageYear = document.getElementById("errorMessageYear");
  const invalidDate = document.getElementById("invalidDate");

  // Ocultar todos los mensajes de error
  errorMessageDay.textContent = "";
  errorMessageMonth.textContent = "";
  errorMessageYear.textContent = "";
  invalidDate.textContent = "";

  // Validar los valores de los campos
  if (!isValidDate(day, month, year)) {
    // Si la fecha no es válida, mostrar un mensaje de error específico para cada campo
    if (!isValidDay(day, month, year)) {
      errorMessageDay.textContent = "Must be a valid day.";
    }
    if (!isValidMonth(month)) {
      errorMessageMonth.textContent = "Must be a valid month";
    }
    if (!isValidYear(year)) {
      errorMessageYear.textContent = "Must be in the past.";
    }
    invalidDate.textContent = "Must be a valid date";
    return; // Detener la ejecución de la función
  }

  // Obtener la fecha actual
  const today = new Date();

  // Crear la fecha de nacimiento
  const birthDate = new Date(year, month - 1, day); // Restar 1 al mes, ya que en JavaScript los meses van de 0 a 11

  // Calcular la diferencia en milisegundos
  let difference = today - birthDate;

  // Convertir la diferencia de milisegundos a años, meses y días
  const ageDate = new Date(difference);

  const years = Math.abs(ageDate.getUTCFullYear() - 1970);
  const months = ageDate.getUTCMonth();
  const days = ageDate.getUTCDate() - 1;

  // Mostrar los resultados en la interfaz de usuario
  const resultsYear = document.getElementById("resultsYear");
  const resultsMonth = document.getElementById("resultsMonth");
  const resultsDay = document.getElementById("resultsDay");

  resultsYear.textContent = `${years}`;
  resultsMonth.textContent = `${months}`;
  resultsDay.textContent = `${days}`;
}

// Agregar el evento de escucha al formulario
const form = document.getElementById("form");
form.addEventListener("submit", logSubmit);

function isValidDate(day, month, year) {
  return isValidDay(day, month, year) && isValidMonth(month) && isValidYear(year);
}

function isValidDay(day, month, year) {
  // Convertir los valores a números enteros
  day = parseInt(day, 10);
  month = parseInt(month, 10);
  year = parseInt(year, 10);

  // Validar que el día esté dentro del rango válido para el mes y el año dados
  const daysInMonth = new Date(year, month, 0).getDate();
  return day >= 1 && day <= daysInMonth;
}

function isValidMonth(month) {
  // Convertir el valor a número entero
  month = parseInt(month, 10);

  // Validar que el mes esté dentro del rango válido (1-12)
  return month >= 1 && month <= 12;
}

function isValidYear(year) {
  // Convertir el valor a número entero
  year = parseInt(year, 10);

  // Validar que el año esté dentro de un rango razonable (por ejemplo, entre 1900 y 2100)
  return year >= 1900 && year <= 2100;
}